let memory = [];


class OperacionesDom {

    principal = this.crearElemento('div', '', 'principal');
    constructor() {
        this.crearElemento();
        document.body.appendChild(this.principal);
    }

    crearElemento(nameElement, clase, id, contenido) {
        let elemento = document.createElement(nameElement);
        elemento.className = clase;
        elemento.id = id;
        elemento.textContent = contenido;
        elemento.value = contenido;
        return elemento;
    }

    appendElement(addElemento) {
        return this.principal.appendChild(addElemento);
    }

    eventListener(element, click, funcion) {
        return element.addEventListener(click, funcion);
    }


}

class Operaciones {    

    constructor() {
        this.primerValor = memory[0];
        this.segundoValor = memory[2];
    }

    static suma() {
        return this.primerValor + this.segundoValor;
    }

    static resta() {
        return this.primerValor - this.segundoValor;
    }

    static multiplicacion() {
        return this.primerValor * this.segundoValor;
    }

    static division() {
        return this.primerValor / this.segundoValor;
    }

}


class CrearCalcu extends OperacionesDom {

    constructor() {
        super();
        this.crearElemento();
        this.resultado();
        this.creandoBtns();
        this.creandoSignos();
    }


    resultado() {
        let resultado = this.appendElement(this.crearElemento('p', '', 'resultado'));
        resultado.textContent = ''
        return resultado
    }

    creandoBtns() {
        let addBtns = this.principal.appendChild(this.crearElemento("div", 'lft', ''));

        for (let i = 0; i < 10; i += 1) {
            addBtns.appendChild(this.crearElemento('button', '', '', i))
                .addEventListener('click', function(e) {
                    resultado.textContent = i;
                    memory.push(parseInt(resultado.textContent))
                    console.log(memory)
                    return memory;
                })
        }

    }

    creandoSignos() {
        let addBtns = this.principal.appendChild(this.crearElemento("div", 'rgth', '')),
            arrOperadores = ['+', '-', '*', '/'];

        for (let i = 0; i < arrOperadores.length; i += 1) {
            addBtns.appendChild(this.crearElemento('button', '', '', arrOperadores[i]))
                .addEventListener('click', function(e) {
                    resultado.textContent = arrOperadores[i];
                    let operador = resultado.textContent;
                    memory.push(operador)
                    console.log(memory)
                    let operadorFinal = memory[1];
                    let primerValor = memory[0];
                    let segundoValor = memory[2];
                    switch (operadorFinal) {
                        case '+':
                            let suma = primerValor + segundoValor
                            resultado.textContent = suma;
                        break;
                        case '-':
                            let resta = primerValor - segundoValor
                            resultado.textContent = resta;
                            break;
                        case '*':
                            let multi = primerValor * segundoValor
                            resultado.textContent = multi;
                            break;
                        case '/':
                            let div = primerValor / segundoValor
                            resultado.textContent = div;
                            break;
                    }
                })

        }

    }

}


let elementos = new CrearCalcu();

let datos = new OperacionesDom();

let operaciones = new Operaciones();