let input1 = document.createElement('p');
let input2 = principal.appendChild(input1).id= 'resultado';
let resultado = document.getElementById('resultado');
let btn = document.getElementsByTagName('button');
class CrearCalcu {
    elements(){
        let principal = document.getElementById('principal');
        
        let teclas = (function () {
            function crearDiv() {
                let creandoDiv = document.createElement("button");  
                return creandoDiv;
            }

            function creandoDivs() {
                let addBtn = document.createElement("div"),
                addPb = principal.appendChild(addBtn),
                divs = [],
                i = 0,
                numDivs = 10;
        
                for (i; i < numDivs; i += 1) {
                    divs.push(crearDiv());
                    addPb.appendChild(divs[i]);
                    divs[i].innerText = i;
                    divs[i].value = i;
                    divs[i].id='num'+i;
                }
                addBtn.className= 'lft';
            }
            creandoDivs();

            function signos() {
                let addBtn = document.createElement("div"),
                addPb = principal.appendChild(addBtn),
                divs = [],
                i = 0,
                numDivs = 4;
        
                for (i; i < numDivs; i += 1) {
                    divs.push(crearDiv());
                    addPb.appendChild(divs[i]);
                    divs[i].id='sim'+i;
                }
                divs[0].innerText = "+";
                divs[1].innerText = "-";
                divs[2].innerText = "*";
                divs[3].innerText = "/";
                divs[0].value = "+";
                divs[1].value = "-";
                divs[2].value = "*";
                divs[3].value = "/";
                addBtn.className= 'rgth';
            }
            signos()
        }());
    }
}

class Eventos {
    constructor (operandoa, operandob, operacion) {
        this.operandoa = operandoa;
        this.operandob = operandob;
        this.operacion = operacion;
    }

    eventos() {
        for (let i=0; i < 10; i++) {
            document.getElementById('num'+i).onclick = function(e){
                resultado.textContent = resultado.textContent  + i;
            }
        }
        for (let i=0; i < 4; i++) {
            document.getElementById('sim'+i).onclick = function(e){
                this.operandoa = resultado.textContent;
                this.operacion = document.getElementById('sim'+i).value;
                resultado.textContent = '';
            }
        }
        this.operandob = resultado.textContent;
    }
}

class Calculadora extends Eventos {
    get events() {
        let res= 0;
        switch(this.operacion){
            case "+":
                res = parseInt(this.operandoa) + parseInt(this.operandob);
                break;
    
            case "-":
                res = parseInt(this.operandoa) - parseInt(this.operandob);
                break;
    
            case "*":
                res = parseInt(this.operandoa) * parseInt(this.operandob);
                break;
    
            case "/":
                res = parseInt(this.operandoa) / parseInt(this.operandob);
                break;   
        }
        return res;
    }
}




let calc = new Calculadora();
calc.operandoa=2;
calc.operandob=3;
let fin = calc.events;
console.log(fin);

let elementos = new CrearCalcu();
elementos.elements();
let evnt = new Eventos();
evnt.eventos();
