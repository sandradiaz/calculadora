let memory = [];



class OperacionesDom {

    principal = this.crearElemento('div', '', 'principal');
    constructor() {
        //this.crearElemento();
        document.body.appendChild(this.principal);
    }

    crearElemento(nameElement, clase, id, contenido) {
        let elemento = document.createElement(nameElement);
        elemento.className = clase;
        elemento.id = id;
        elemento.textContent = contenido;
        elemento.value = contenido;
        return elemento;
    }

    appendElement(addElemento) {
        return this.principal.appendChild(addElemento);
    }

    eventListener(element, click, funcion) {
        return element.addEventListener(click, funcion);
    }


}

class Operaciones {
    suma(a, b) {
        return a + b;
    }

    resta(a, b) {
        return a - b;
    }

    multiplicacion(a, b) {
        return a * b;
    }

    division(a, b) {
        if (b !== 0) {
            return a/b;
        } else {
            return a/1;
        }
    }
}


class CrearCalcu extends OperacionesDom {

    constructor() {
        super();
        this.resultado();
        this.creandoBtns();
        this.creandoSignos();
        this.hacerOperacion();
        this.operaciones = new Operaciones();
    }

    resultado() {
        let resultado = this.appendElement(this.crearElemento('p', '', 'resultado'));
        resultado.textContent = '';
        return resultado;
    }

    creandoBtns() {
        let addBtns = this.principal.appendChild(this.crearElemento("div", 'lft', ''));

        for (let i = 0; i < 10; i += 1) {
            addBtns.appendChild(this.crearElemento('button', '', '', i))
                .addEventListener('click', function(e) {
                    resultado.textContent = i;
                    memory.push(parseInt(i));
                    console.log(memory);
                    return memory;
            })
        }

    }

    creandoSignos() {
        let addBtns = this.principal.appendChild(this.crearElemento("div", 'rgth', '')),
            arrOperadores = ['+', '-', '*', '/'];            

        for (let i = 0; i < arrOperadores.length; i += 1) {
            addBtns.appendChild(this.crearElemento('button', '', '', arrOperadores[i]))
                .addEventListener('click', (function(e) {
                    this.hacerOperacion(arrOperadores[i]);
            }).bind(this));
        }
    }

    hacerOperacion(operador) {
        switch (operador) {
            case '+':
                if (memory.length === 1){
                    memory[1] = 0;
                    resultado.textContent = this.operaciones.suma(memory[0], memory[1]);
                    memory[0] = (parseInt(resultado.textContent));                    
                }                 
                resultado.textContent = this.operaciones.suma(memory[0], memory[1]);
                memory = [];
                memory = [parseInt(resultado.textContent)];
            break;
            
            case '-':
                if (memory.length === 1){
                    memory[1] = 0;
                    resultado.textContent = this.operaciones.resta(memory[0], memory[1]);
                    memory[0] = (parseInt(resultado.textContent));                    
                }                 
                resultado.textContent = this.operaciones.resta(memory[0], memory[1]);
                memory = [];
                memory = [parseInt(resultado.textContent)];
            break;

            case '*':
                if (memory.length === 1) {
                    memory[1] = 1;
                    resultado.textContent = this.operaciones.multiplicacion(memory[0], memory[1]);
                    memory[0] = (parseInt(resultado.textContent));                    
                }                 
                resultado.textContent = this.operaciones.multiplicacion(memory[0], memory[1]);
                memory = [];
                memory = [parseInt(resultado.textContent)];
            break;
            
            case '/':
                if (memory.length === 1) {
                    memory[1] = 1;
                    resultado.textContent = this.operaciones.division(memory[0], memory[1]);
                    memory[0] = (parseInt(resultado.textContent));                    
                }                 
                resultado.textContent = this.operaciones.division(memory[0], memory[1]);
                memory = [];
                memory = [parseInt(resultado.textContent)];
            break;       
        } 
    }


}


let elementos = new CrearCalcu();

// se quita porque extinde de CrearCalcu, por eso se duplica 'principal'
//let datos = new OperacionesDom();

